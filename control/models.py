from django.conf import settings
from django.db import models


class Symbol(models.Model):
    symbol = models.CharField(verbose_name="Par de Moedas", max_length=15)

    def __str__(self):
        return self.symbol


class Profile(models.Model):
    title = models.CharField(verbose_name="Titulo", max_length=100)
    symbol = models.ForeignKey(Symbol, on_delete=models.CASCADE)
    candlesize = models.IntegerField(verbose_name="Candle em minutos")

    macd_period = models.IntegerField(verbose_name="MACD Período")
    ema_period = models.IntegerField(verbose_name="EMA Período")
    rsi_period = models.IntegerField(verbose_name="RSI Período")

    ema_curta = models.IntegerField(verbose_name="EMA Curta")
    ema_longa = models.IntegerField(verbose_name="EMA Longa")

    rsi = models.IntegerField(verbose_name="RSI")
    rsi_alta = models.IntegerField(verbose_name="RSI Altar")
    rsi_baixa = models.IntegerField(verbose_name="RSI Baixa")

    macd_curta = models.IntegerField(verbose_name="MACD Curta")
    macd_longa = models.IntegerField(verbose_name="MACD Longa")
    macd_signal = models.IntegerField(verbose_name="MACD Signal")
    macd_alta = models.FloatField(verbose_name="MACD Alta")
    macd_baixa = models.FloatField(verbose_name="MACD Baixa")

    created_at = models.DateTimeField('Criado em', auto_now_add=True)
    updated_at = models.DateTimeField('Atualizado em', auto_now=True)
    active = models.BooleanField(verbose_name='Publicar', default=True)
    added_by = models.ForeignKey(settings.AUTH_USER_MODEL,
                                 null=True,
                                 blank=True,
                                 on_delete=models.SET_NULL)

    class Meta:
        db_table = 'profile'

    def __str__(self):
        return self.title

    def save_model(self, request, obj, form, change):
        obj.added_by = request.user
        print("usuario corrente")
        print(request.user)
        super().save_model(request, obj, form, change)
