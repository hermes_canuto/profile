from django.contrib import admin

from .models import Profile, Symbol


class ProfileAdmin(admin.ModelAdmin):
    list_display = ['title', 'created_at', 'added_by']
    list_display_links = ('title', 'created_at')
    list_filter = ['active', ]
    exclude = ['added_by', ]
    ordering = ['created_at']

    def save_model(self, request, obj, form, change):
        obj.added_by = request.user
        print("usuario corrente")
        print(request.user)
        super().save_model(request, obj, form, change)


admin.site.register(Profile, ProfileAdmin)

admin.site.register(Symbol)
