from django.db import models


class Exchange(models.Model):
    exchange = models.CharField(max_length=45, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'exchange'


class HistoricalDoubleValue(models.Model):
    exchange = models.ForeignKey(Exchange, models.DO_NOTHING)
    value = models.DecimalField(max_digits=15, decimal_places=10, blank=True, null=True)
    symbol = models.CharField(max_length=10, blank=True, null=True)
    timestamp = models.DateTimeField(blank=True, null=True)
    create_time = models.DateTimeField(blank=True, null=True)
    result = models.TextField(blank=True, null=True)  # This field type is a guess.

    class Meta:
        managed = False
        db_table = 'historical_double_value'
        unique_together = (('id', 'exchange'),)


class TbTechnicalAnalysisLog(models.Model):
    log = models.TextField(blank=True, null=True)  # This field type is a guess.
    create_time = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'tb_technical_analysis_log'
